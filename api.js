/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * api.js contains interactions with the Eris discord api
 */
const Eris = require('eris');
const screen = require('./screen.js');
const chat = require('./chat.js')
const menu = require('./menu.js');
const model = require('./model.js')
const config = require('./config.json');

var client = new Eris(config.token);

client.on('ready', () => { // client events have to go here because api is already referenced by controller and cannot reference contro  
    chat.bezel.setContent("Press tab to pick a channel");
    chat.main.pushLine("Logged in as {bold}" + client.user.username + "{/}");
    chat.main.pushLine("\n{bold}Type{/} {white-bg}{black-fg}/help{/white-bg}{/black-fg} {bold}to get started with using minicord.{/}\n"); // this has to go here because commands need to reference api, so api cannot reference commands
    chat.main.pushLine("If you encounter any bugs or issues when using this program, please let me know at {bold}https://gitlab.com/zaema/minicord/issues {/}\n");
    screen.screen.render();
    model.unlock = true; // unlock inputs once logged in
});

client.on("messageCreate", (msg) => {
    if (msg.channel.id == model.currentChannelId) {
	// check if the message is sent in the channel that the client is currently in
	
	// Don't think embeds need to be checked as most of their links print correctly.
	if (model.historyIndex < 1) { // check if the client is reading the history of the channel.
	    if (msg.attachments.length > 0) {
		var s = "{bold}" + msg.author.username + ":{/bold} ";
		if (msg.cleanContent != "") { // also need to check if the attachment has a message attached to it.
		    s += msg.cleanContent + "\n";
		}
		for (var e = 0; e < msg.attachments.length; ++e) {
		    s += msg.attachments[e].url;
		}
		chat.main.pushLine(s);
	    } else {
		chat.main.pushLine("{bold}" + msg.author.username + ":{/bold} " + msg.cleanContent);
	    }
	}
	
	if (msg.author.id == client.user.id) { // highlight chat label when new message received
	    chat.main.setScrollPerc(100); // scroll to bottom if the message is sent by the user
	    chat.bezel.setContent(model.guildName + " - " + model.channelName);
	    // screen.render(); for some reason the screen.render outside the bracket works.
	} else {
	    chat.bezel.setContent("{white-bg}{black-fg}" + model.guildName + " - " + model.channelName + "{/}");
	}
	screen.screen.render();
    }
});

client.on("messageUpdate", (msg) => { // When a message is edited
    // for now it most likely will just push the message with *, similar to how edits are shown on Riot.im mobile
    if (msg.channel.id == model.currentChannelId) {
	if (model.historyIndex < 1) {
	    chat.main.pushLine("{bold}" + msg.author.username + ": *{/bold}" + msg.cleanContent); // TODO: modify this to turn embed images into web links that users can open in browser.
	    screen.screen.render();
	}
	chat.bezel.setContent("{white-bg}{black-fg}" + model.guildName + " - " + model.channelName + "{/}");
    }
});

async function fetchHistory() {
    //the searches have to be nested into each other, otherwise they cannot execute sequentially.
    var query = { // fetch second half of the 50 to be fetched messages
	offset: model.historyIndex * 50 + 25,
	contextSize: 0, // dont fetch any messages around the messages
	limit: 25
    };
    let signal = await client.searchChannelMessages(model.currentChannelId, query).then(async function(result) {
	// function has to be async since there is another await encapsulated
	var lastHalf = result.results;
	
	query = { // all query parameters have to be defined again, or there will be duplicate/inconsistent messages (which for some reason did not happen before when the functions were not async ones)
	    offset: model.historyIndex * 50,
	    contextSize: 0,
	    limit: 25
	};
	
	let signalCascade = await client.searchChannelMessages(model.currentChannelId, query).then(function(first) {
	    // var firstHalf = result.results;
	    // replaced the var with the direct returned object from the promise, to prevent
	    // asynchronous execution causing errors.

	    // the if and elseif statements also have to be nested within the promise.
	    if (lastHalf.length == 0 && first.results.length == 0) {
		return Promise.resolve(1); // return 1 (no more messages) and store it in signalCascade
	    } else if (lastHalf.length == 0) { // the last half is empty i.e. there remains less than 25 messages at the top

		chat.input.setValue("Viewing " + (model.historyIndex * 50) + " to " + (model.historyIndex * 50 + first.results.length) + " messages in the past"); // shows the precise message range
		chat.main.setContent("");
		pushHistory(first.results);
		
		return Promise.resolve(0); // return 0 (success) and store it in signalCascade
		
	    } else { // when both firsthalf and lasthalf contains messages

		chat.input.setValue("Viewing " + (model.historyIndex * 50) + " to " + (model.historyIndex * 50 + 25 + lastHalf.length) + " messages in the past"); // lastHalf.length is usually 25, however if it is less, this allows the range to show the precise number of messages that is rendered.
		chat.main.setContent("");
		var e = first.results.concat(lastHalf);
		pushHistory(e);
		
		return Promise.resolve(0); // return 0 (success) and store it in signalCascade
	    }
	}).catch(async function() { //
	    return Promise.resolve(2); // return 2 (api failure) and store it in signalCascade
	}); // returns different failure signal (2) if any of the api calls fail
	
	return signalCascade; // returns 0 or 1 and store it in signal
	
    }).catch(function() { // function has to be async since there is another await encapsulated
	return Promise.resolve(2); // return 2 (api failure) and store it in signal
    });
    return signal; // returns 0, 1, or 2
}

async function getCurrentChannelMessages() {
    let signal = model.currentChannel.getMessages().then(function(result) {
	renderMessages(result); // have to print to the ui from this file, since javascript can't seem to be able to return large objects/arrays properly
	return Promise.resolve(0); // return 0 and store it in signal
    }).catch(function() { // error will occur in chat interface if user gets kicked while trying to fetch latest batch of messages.
	return Promise.resolve(1); // return 1 and store it in signal
    });
    return signal; // returns 0 or 1
}

async function createMessage(content) {
    let signal = client.createMessage(model.currentChannelId, content).then(function(result) {
	return Promise.resolve(0); // returns 0 (success) and store it in signal
    }).catch(function () {	
	return Promise.resolve(1);// returns 1 (fail) and store it in signal
    });
    return signal; // returns 0 or 1
}



// menu interactions =====================================================



async function listServers() {  // clear guild list and rerender. this is called only when toggle occurs
    menu.serverList.clearItems();
    var guilds = ["PMs"]; // first item will contain PMs and group chats
    model.guildIds = ["PMs"];   // define the first item in guildIds array as PMs since that will be compared with the string not the id
    guilds = guilds.concat(client.guilds.map(g => g.name));
    // appends array of server names to the servers array
    model.guildIds = model.guildIds.concat(client.guilds.map(g => g.id));
    // append array of server ids to the guildid array
    for (var i = 0; i < guilds.length; ++i) {
	menu.serverList.pushItem(guilds[i]);
    }
    screen.screen.render();
    return Promise.resolve();
}

function listPms() {
    model.currentGuild = null;
    
    // TODO: update the listPms function to also store PMs and group chats as objects inside model.channels
    
    menu.channelList.clearItems();
    model.channelIds.length = 0; // clear all channels before fetching PMs
    model.pmTypes.length = 0; // clear the pmTypes array as it will be filled again
    var names = client.groupChannels.map(g => g.name); // TODO: find a more efficient way to do this
    var pmUsers = client.privateChannels.map(g => g.recipient.username);
    if (names.length == 0 && pmUsers.length == 0) { // Don't ping the API too much if there are no pms
	menu.channelList.pushItem("{bold}The list is empty");
	screen.screen.render();
    } else {
	var id = client.groupChannels.map(g => g.id);
	for (var i = 0; i < names.length; ++i) {
	    if (names[i] == null) { // if the group chat doesn't have a name, give it a temp name
		names[i] = "Unnamed Group Chat"; // TODO: get usernames of the participants and use it as the name
	    }
	    model.pmTypes.push(false); // push false boolean to pmTypes array for checking for the channelList click.on event
	}
	
	names = names.concat(pmUsers); // concat the PM Users to the exist array of group channels.
	var pmid = client.privateChannels.map(g => g.id); // get array of pm IDs
	for (var i = 0; i < pmid.length; ++i) {
	    model.pmTypes.push(true); // push true boolean to pmTypes array for checking for the channelList click.on event
	}
	
	id = id.concat(pmid);
	for (var i = 0; i < id.length; ++i) {
	    menu.channelList.pushItem(names[i]);
	    model.channelIds.push(id[i].toString()); // reuse the channelIds array for entering PMs as well.
	}
	screen.screen.render();
    }
}

function listChannels(itemId) {
    model.currentGuild = client.guilds.find(g => g.id == model.guildIds[itemId]); // define currentGuild as an object with name that matches the name of the guild clicked on.
    
    menu.channelList.clearItems(); // clear channelList visually

    model.channels.length = 0; // clear the channels objects array so they can be fetched for a new server
    
    var names = model.currentGuild.channels.map(g => g.name);  // get array with name of channels
    if (names.length == 0) { // dont try to fetch channels that dont exist
	menu.channelList.pushItem("{bold}The list is empty");
	screen.screen.render();
    } else { // if the guild has more than 0 channels, print them
	var id = model.currentGuild.channels.map(g => g.id);       // get array with the channel ids
        var typeId = model.currentGuild.channels.map(g => g.type); // get array with typenum of channels
        var positions = model.currentGuild.channels.map(g => g.position); // array of positions (based on arrangement by server admins)
        var parentId = model.currentGuild.channels.map(g => g.parentID); // array containing the category parents
        var categoryHeaders = []; // will be storing array containing channels that are actually category headers. They will be iterated to determine the position of categorized channels for sorting

        /*
         * TODO: A newly created discord server will have 2 categories with 
         * the *EXACTLY MATCHING* position values (0). This will cause the 
         * category headers to pile on top of each other at the top of the 
         * list.
         */

	for (var i = 0; i < id.length; ++i) { // initialize channels array
            var channel = {
                name: names[i],
                id: id[i],
                type: typeId[i],
                position: positions[i],
                parent: parentId[i]
            }
            model.channels.push(channel);
        }

	for (var i = 0; i < model.channels.length; ++i) { // first obtain an array of category objects
            if (model.channels[i].type == 4) {
                categoryHeaders.push(model.channels[i]);
                model.channels[i].parent = null; // by default category headers have an undefined instead of null parent id, which is very difficult to check for. Hence null is set here instead to allow easier sorting later
            }
        }

	/*
         * Javascript apparently does not support iterating through objects in for 
         * loop in the style of:
         *      for (object in array)
         * so the traditional numeric method has to be used.
         */
        
        categoryHeaders.sort(function(a, b) { // sort category headers by their position
            return a.position - b.position;
        });

	for (var i = 0; i < model.channels.length; ++i) { // iterate through channels and change parent to the category index.
            if (model.channels[i].parent == null) { // the channel is either a header, or uncategorized
                if (model.channels[i].type === 4) {// if the channel is a header, the position value will be stored in the parent value, and this will be used to sort all channels later.
                    model.channels[i].parent = model.channels[i].position;
                    model.channels[i].position = -1; // set position as -1, so that it does not have conflicting positions with the first channel in a category.
                    
                } else { // an actual channel, which is uncategorized
                    model.channels[i].parent = -1; // set the parent as a fake category with index -1, so that it will be put in front of everything else
                }
            } else { // it is a channel that belongs to a category, so the index of the category that it belongs to will need to be determined.
                for (var j = 0; j < categoryHeaders.length; ++j) { // iterate through all category headers, comparing each id with the channel's parent id
                    if (model.channels[i].parent == categoryHeaders[j].id) {
                        model.channels[i].parent = j; // set the parentid as an index which shows the category it belongs to
                        break; // break from the inner loop
                    }
                }
            }
        }

	model.channels.sort(function(a, b) {
            if (a.parent < b.parent) { // at this point the parent values have integers indicating the hierarchy of the categories they belong to. If the categories are different, then the lower category will always be put first. And since the uncategorized channels have a parent value of -1, they will be put ahead of everything else.
                return -1; // a's parent is 'smaller'
            } else if (a.parent > b.parent) {
                return 1; // b's parent is 'smaller'
            } else { // parents are equal; the channels belong in the same category
                if (a.position < b.position) {
                    return -1
                } else { // it is not possible for the parents and position to be equal at the same time, so do not need to check for a.pos < b.pos
                    return 1;
                }
            }
        });
	
	/* 
	 * finally, push the channels to the GUI
	 */
	
	for (var i = 0; i < model.channels.length; ++i)
	    switch(model.channels[i].type) {
		
	    case 2: // voice channel
		menu.channelList.pushItem("<( " + model.channels[i].name); // append a loudspeaker icon
		break;

	    case 4: // channel category header
		menu.channelList.pushItem("==={bold}{underline}_" + model.channels[i].name + "_{/}==="); // append equal signs, and make the text bold and underlined
		break;

	    default: // text or announcement (0 or 5), unsure what type 3 is
		menu.channelList.pushItem(model.channels[i].name);
		break;	
	    }
	
	screen.screen.render();
    }
}

async function enterChannel(itemId) {
    if (model.currentGuild != null) { // if the channels are not PMs
	model.currentChannelId = model.channels[itemId].id; // store the clicked on channel's id so it can be called later to check for messageCreate events.
	/* Find the channel item which has id that matches with the id within the channelList array
	   with the index of the clicked item. then call the getMessages() function which returns a
	   promise for an array between the size of 0 to 50 containing message objects, then() await
	   the completion of the promise and pass the resulting message objects array (result) to a
	   function */
	model.currentChannel = model.currentGuild.channels.find(g => g.id == model.currentChannelId);
	let signal = await model.currentChannel.getMessages().then(function(result) {
	    renderMessages(result);
	    return Promise.resolve(0); // returns 0 and store it in signal
	}).catch(function() {
	    // temporary workaround that prevents crashing upon clicking a channel. 
	    // TODO: listChannels should only print out accessible channels 
	    return Promise.resolve(1);  // return 1 and store it in signal
	});
	return signal; // returns 1 or 0 to controller
    } else { // This occurs if currentGuild == null i.e. the user is viewing the PMs
	model.currentChannelId = model.channelIds[itemId];
	if (model.pmTypes[itemId]) { // if its a PM
	    model.currentChannel = client.privateChannels.find(g => g.id == model.channelIds[itemId]);
	    let signal = await model.currentChannel.getMessages().then(function(result) {
		renderMessages(result); // placeholder value to make it wait
		return Promise.resolve(0); // returns 0 and store it in signal
	    }); // highly unlikely for pms to be inaccessible
	    return signal; // returns 0 or 1
	    
	} else { // if its a group chat
	    model.currentChannel = client.groupChannels.find(g => g.id == model.channelIds[itemId]);
	    let signal = await model.currentChannel.getMessages().then(function(result) {
		renderMessages(result);
		return Promise.resolve(0);
	    }).catch(function() { // if group chat is inaccessible (just got kicked)
		return Promise.resolve(1);
	    });
	    return signal; // returns 1 or 0 to controller
	}
    }
}

/* While the Eris.js framework allows joining servers via invite links when using a user token,
 * the request signature and headers sent from the join request will be flagged by discord's
 * centralized servers as a bot and rejected. Hence it is not possible to join servers without
 * spoofing the request headers, which I do not know how to do yet.
 *
async function acceptInvite(inviteId) {
    var signal = await client.acceptInvite(inviteId).then(function(invite) {
	return Promise.resolve(invite.guild.name);
    }).catch(function() {
	return Promise.resolve(1);
    });
    return Promise.resolve(signal.toString()); // for some reason signal has to be converted to string, otherwise it will disappear.
}*/

/* NOT EXPORTED */ /* NOT EXPORTED */ /* NOT EXPORTED */ /* NOT EXPORTED */ /* NOT EXPORTED */
function pushHistory(messages) { // messages are contained in an array of size 1 due to the fact that by default the search function returns surrounding messages
    for (var i = messages.length-1; i >= 0; --i) {
	if (messages[i][0].attachments.length > 0) { // check for attachments then print their URL
	    var s = "{bold}" + messages[i][0].author.username + ":{/bold} ";
	    if (messages[i][0].cleanContent != "") { // also need to check if the attachment has a message attached to it.
		s += messages[i][0].cleanContent + "\n";
	    }
	    for (var e = 0; e < messages[i][0].attachments.length; ++e) {
			    s += messages[i][0].attachments[e].url;
	    }
	    chat.main.pushLine(s);
	} else {
	    chat.main.pushLine("{bold}" + messages[i][0].author.username + ":{/bold} " + messages[i][0].cleanContent);
	}
    }
}

/* NOT EXPORTED */
function renderMessages(result) {
    model.historyIndex = 0; // set to 0, this is either called when first entering a channel, or when scrolling down to the present, or sending a message while viewing history. All of these cases should set historyIndex to 0.
    chat.main.setContent(""); // since renderMessages is called not only when first entering the channel,
    // but also when sending messages while viewing the history and when going down to the present from
    // viewing the channel history, it should always clear the contents of main.
    for (var i = result.length-1; i >= 0; --i) {
	// Don't think embeds need to be checked as most of their links print correctly.
	if (result[i].attachments.length > 0) { // check for attachments then print their URL
	    var s = "{bold}" + result[i].author.username + ":{/bold} ";
	    if (result[i].cleanContent != "") { // also need to check if the attachment has a message attached to it.
		s += result[i].cleanContent + "\n";
	    }
	    for (var e = 0; e < result[i].attachments.length; ++e) {
			    s += result[i].attachments[e].url;
	    }
	    chat.main.pushLine(s);
	} else {
	    chat.main.pushLine("{bold}" + result[i].author.username + ":{/bold} " + result[i].cleanContent);
	}
    }
    chat.main.focus();
    chat.main.setScrollPerc(100);
    screen.screen.render();
}



module.exports = { client, fetchHistory, getCurrentChannelMessages, createMessage, listServers, listPms, listChannels, enterChannel }

/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * screen.js contains the basic screen node of the blessed module
 */
const blessed = require('blessed');
const config = require('./config.json');

var screen;

newScreen(); // need to call it at least once to initialise the screen for the first time 

function newScreen() { // constructor for the screen
    screen = blessed.screen({ // construct the blessed screen
	smartCSR: true,
	autoPadding: true,
	dockBorders: true,
	cursor: {
	    artificial: true,
	    shape: 'block',
	    blink: true
	},
	fullUnicode: config.fullunicode, // you should modify this value in config.json
	ignoreLocked: ['C-c', 'escape']
	// make sure ctrl-c will destroy the screen no matter what
	// make sure escape will fully escape the input box and immediately allow scrolling
    });
}

module.exports = { newScreen, screen }

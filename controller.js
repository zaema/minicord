/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * controller.js contains general events for blessed modules
 */
const screen = require('./screen.js');
const chat = require('./chat.js');
const menu = require('./menu.js');
const model = require('./model.js');
const api = require('./api.js');

chat.menuButton.on('click', function () { // interface switching via buttons.
    if (model.unlock) {
	model.unlock = false; // lock inputs
	toggleInterface();
    }
});

menu.backButton.on('click', function () { // not for mobile as touch detection is very iffy on Termux
    if (model.unlock) {
	model.unlock = false; // lock inputs
	toggleInterface();
    }
});

screen.screen.key('tab', function () {
    if (model.unlock) {
	model.unlock = false; // lock inputs
	toggleInterface();
    }
});

screen.screen.key('C-c', function () { // control + c will kill the screen to exit the program
    screen.screen.destroy();
    process.exit(0);
});

function toggleInterface() {
    if (chat.main.hidden) {
	menu.menuBezel.hide();
	menu.serverList.hide();
	menu.channelList.hide();
	chat.bezel.show();
	chat.main.show();
	chat.textbox.show();
	chat.main.focus();       // focuses the main list to allow immediate scrolling
	screen.screen.render();
	model.unlock = true; // allow input after everything is rendered
    } else {
	chat.bezel.hide();
	chat.main.hide();
	chat.textbox.hide();
	api.listServers().then(function(){      // rerender server list with current servers the user is in.
	    menu.menuBezel.show();
	    menu.serverList.show();
	    menu.channelList.show();
	    menu.serverList.focus(); // focuses the server list so user can immediately cycle through servers.
	    screen.screen.render();
	    model.unlock = true; // allow input after everything is rendered
	});
    } 
}

module.exports = { toggleInterface }

/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * chatcontroller.js contains events and interactions for the chat interface
 */
const screen = require('./screen.js');
const chat = require('./chat.js');
const menu = require('./menu.js');
const api = require('./api.js');
const model = require('./model.js');
const commands = require('./commands.js');

screen.screen.key('enter', function () { // this will focus the input box
    if (menu.menuBezel.hidden && model.unlock) {
	if (model.historyIndex > 0) {
	    chat.input.clearValue(); // clear the input box as it was used to display the history viewing progress
	}
	chat.input.focus();
    }
    // https://github.com/chjj/blessed/issues/45  \r is always sent, no way to determine key combos
    // https://github.com/chjj/blessed/issues/329 input.submit(); seems to not work for textareas
});

screen.screen.key('escape', function () { // make sure esc properly escapes the input box
    if (menu.menuBezel.hidden) {
	chat.input.cancel(); // both of these are needed to ensure that scrolling can immediately work after
	chat.main.focus();   // pressing esc a single time
	screen.screen.render();
    }
});

screen.screen.key('up', function () {
    if (model.currentChannelId != null && model.unlock) {
	if (menu.menuBezel.hidden && chat.main.getScrollPerc().toString() == "0") {  // called when scrolled to very top in chat interface
	    if (model.historyIndex >= 99) { // 99 * 50 + 25 = 4975, any more increment of search offset will be ignored by the API
		chat.input.setValue("You've reached the maximum history limit (5000) of the API");
		screen.screen.render();
	    } else {
		++model.historyIndex;
		fetchHistory("up");
	    }
	}
    }
});

screen.screen.key('C-p', function () {
    if (model.currentChannelId != null && model.unlock) {
	if (menu.menuBezel.hidden) {
	    if (model.historyIndex >= 99) {
		chat.input.setValue("You've reached the maximum history limit (5000) of the API");
		screen.screen.render();
	    } else {
		++model.historyIndex;
		fetchHistory("up");
	    }
	}
    }
});

screen.screen.key('down', function () { // called when scrolled to very bottom in chat interface
    if (model.currentChannelId != null && model.unlock) {
	if (menu.menuBezel.hidden && chat.main.getScrollPerc().toString() == "100" && model.historyIndex > 0) {
	    if (model.historyIndex === 1) { // scrolling down one more will get to the present, so go to the present instead of searching history
		model.unlock = false;
		chat.input.setValue("Travelling to the present day, present time...");
		screen.screen.render(); // render the loading message

		api.getCurrentChannelMessages().then(function(result) {
		    chat.bezel.setContent(model.guildName + " - " + model.channelName); // unhighlight label
		    if (result === 0) { // if this returns success (0)
			model.unlock = true; // unlock controls
			chat.input.clearValue();
			screen.screen.render();
		    } else { // this can only happen if user is kicked while viewing history (api will return failed (1))
			chat.main.setContent("");
			chat.main.pushLine("{bold}Failed to fetch messages. You may have been kicked from the channel.");
			model.unlock = true;
			screen.screen.render();
		    }
		});
		
	    } else { // this should iterate 50 messages forward towards the present
		--model.historyIndex;
		fetchHistory("down");
	    }
	}
    }
    if (chat.main.getScrollPerc().toString() == "100" && model.unlock) { // check for scrolling to the bottom to remove new message notification
	chat.bezel.setContent(model.guildName + " - " + model.channelName);
    }
});

screen.screen.key('C-n', function () { // a copy of the down arrow event, without the scrollPercent check
    // down arrow combinations cannot be catched on termux, hence an emacs binding is used here
    // this is a temporary workaround for when fetchHistory does not generate enough messages to make
    // the box scrollable (hence scrollpercent will always be 0
    // TODO: Find a way to check if the user cannot scroll down due to insufficient messages
    // this can be hard to do due to the different screen size
    if (model.currentChannelId != null && model.unlock) {
	if (menu.menuBezel.hidden && model.historyIndex > 0) {
	    if (model.historyIndex == 1) { // scrolling down one more will get to the present, so go to the present instead of searching history
		model.unlock = false;
		chat.input.setValue("Travelling to the present day, present time...");
		screen.screen.render(); // render the loading message

		api.getCurrentChannelMessages().then(function(result) {
		    chat.bezel.setContent(model.guildName + " - " + model.channelName); // unhighlight label
		    if (result === 0) { // if this returns success (0)
			model.unlock = true; // unlock controls
			chat.input.clearValue();
			screen.screen.render();
		    } else { // this can only happen if user is kicked while viewing history (api will return failed (1))
			chat.main.setContent("");
			chat.main.pushLine("{bold}Failed to fetch messages. You may have been kicked from the channel.");
			model.unlock = true;
			screen.screen.render();
		    }
		});
		
	    } else { // this should iterate 50 messages forward towards the present
		--model.historyIndex;
		fetchHistory("down");
	    }
	}
    }
    if (chat.main.getScrollPerc().toString() == "100" && model.unlock) {
	chat.bezel.setContent(model.guildName + " - " + model.channelName);
    }
});

screen.screen.on('wheeldown', function() {
    if (chat.main.getScrollPerc().toString() == "100" && model.unlock) { // check for scrolling to the bottom to remove label highlight
	chat.bezel.setContent(model.guildName + " - " + model.channelName);
    }
});

chat.input.on('submit', function (content) { // sending messages. You need to press esc in order to actually
    if (content.trim() == "") {         // escape from the textbox.
	// do nothing if content only contains whitespace, as it will break eris
	chat.input.clearValue();
    } else if (content[0] == '/') {
	content = content.slice(1).trim(); // format command to trim all whitespace. args will be split in the commands file
	commands.processCommand(content);
    } else if (model.currentChannelId != null) {
	/* If user is trying to send a message while viewing the history, go to the present */
	if (model.historyIndex > 0) {
	    model.unlock = false;
	    chat.input.setValue("Travelling to the present day, present time...");
	    screen.screen.render(); // render the loading message

	    api.getCurrentChannelMessages().then(function(result) {
		if (result === 0) { // if this returns success (0)
		    model.unlock = true; // unlock controls
		    chat.input.clearValue();
		    screen.screen.render();
		} else { // this can only happen if user is kicked while viewing history (api will return failed (1))
		    chat.main.setContent("");
		    chat.main.pushLine("{bold}Failed to fetch messages. You may have been kicked from the channel.");
		    model.unlock = true;
		    screen.screen.render();
		}
	    });
	}

	/* message creation api call */
	api.createMessage(content).then(function(result) { // result is either 0 or 1
	    if (result === 1) { // checks if returned number is a failed signal (1). The message will be created if it is successful.
		chat.main.pushLine("{bold}You either don't have permissions to post in this channel, or you are affected by slowmode.");
		chat.main.setScrollPerc(100); // setScrollPerc outside the bracket does not work.
	    }
	});
	
	chat.input.clearValue();
	chat.main.setScrollPerc(100);
	screen.screen.render();
    } else {
	chat.main.pushLine("{bold}You scream into the void, but nothing responds.");
	chat.input.clearValue();
	chat.main.focus();
	chat.main.setScrollPerc(100);
	screen.screen.render();
    }
});

function fetchHistory(direction) {
    model.unlock = false; // prevent unnecessary repeated api calls
    chat.input.setValue("Busy fetching history....");
    screen.screen.render(); // render loading msg

    api.fetchHistory().then(function(result) { // result = 0, 1 or 2
	if (result === 0) { // api returns 0 (meaning success)
	    if (direction == "up") {
		chat.main.setScrollPerc(100); // set the screen to the bottom of the 50 fetched history messages
		screen.screen.render();
	    } else {
		chat.main.setScrollPerc(0); // set the screen to the top of the 50 fetched, since the user would have came from the bottom of the previous 50 history messages
		screen.screen.render();
	    }
	    model.unlock = true;
	} else if (result === 1) { // result is 1 (no more messages)
	    if (direction == "up") { // do nothing except reducing historyIndex to counteract increase
		--model.historyIndex;
		chat.input.setValue("You've reached the top! Do Ctrl-n to go down quickly.");
		// hints the user to use the special shortcut to iterate back to present.
		// hoever, they would have to go up first in order to see it
		// TODO: Find a way to check if the user cannot scroll down due to insufficient messages
	    } else {
		// don't decrease historyIndex because direction is down and historyIndex was already decreased.
		chat.input.setValue("Something terrible happened");
		/* There is a case for when direction is down and both searches are empty, because
		   it can occur when the user is at the start of the history when another person
		   prunes hundreds of messages. This would leave historyIndex in the middle of
		   nowhere with no messages within 50 sides downwards either. in this case it would
		   help decrease the historyIndex until it finds the first messages. */
	    }
	    model.unlock = true;
	    screen.screen.render();	
	} else { // result is 2 (api fails)
	    chat.input.setValue("Failed to fetch messages, please try again.");
	    
	    if (direction == "up") // counteract the change of historyIndex
		--model.historyIndex;
	    else
		++model.historyIndex;
	    
	    model.unlock = true;
	    screen.screen.render();	
	}
    });
}


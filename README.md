# Minicord - A lightweight discord terminal client for android

### NOTE: Unfortunately due to the increasingly authoritarian enforcement of the discord TOS by the discord developers, as of now you cannot login using a user token. However, bot tokens will still work.

Minicord is a command line discord client, build using the 'Eris' discord API and the 'Blessed' node.js module. This allows it to be installed and ran on the Termux emulator on Android without any errors, compared to discord.js clients which cannot be installed properly on Termux due to the non-standard file hierarchy.

<div align="center">

![](example.webm)
</div>

While minicord is designed for usage on mobile devices, you can also use it without any problems on the desktop.

## Installation and Setup
```
git clone https://gitlab.com/zaema/minicord && cd minicord
npm install blessed
npm install --no-optional eris
```
Then, open `config.json` in an editor of your choice, and replace `YOUR_TOKEN` with your discord user token.

**Note:** By default, support for full unicode is disabled in `config.json`. If you often use discord in languages that require full unicode support, you should change the `"fullunicode"` value in config.json from `false` to `true`.

Now you can do
```
node index.js
```
to launch the client.

I will probably figure out a better way to distribute minicord, such as publishing it to npm in the future.

## Usage
`Up/Down arrow keys` will move the chat interface one line at a time. `Scroll wheel` or `touch inputs` will scroll the chat.

`Tab` will toggle between the chat and the server menu.

`Arrow/Emacs/Vim keys` can be used to navigate the server/channel menu.

`Enter` will confirm the hovered server/channel selection in the server menu and focus the input box in the chat menu.

Pressing `Enter` while focusing the input box will send the inputted text to the current channel.

Pressing the `Up` arrow key while scrolled to the top will fetch past messages, 50 at a time. Pressing the `Down` arrow key wile scrolled to the bottom of the messages list will fetch 50 messages 'forward', in the opposite direction of the past messages.

`Ctrl` + `p` and `Ctrl` + `n` will fetch the messages in the message history immediately without needing to scroll to the top or bottom.

## Implemented and Planned Features
Implemented:
- [x] Viewing and sending messages in channels
- [x] Viewing and sending messages in private and group chats
- [x] Viewing up to 5000 past messages within a channel
- [x] Sending single-line messages
- [x] Parsing attachments to urls

Planned:
- [ ] Sending files to a channel
- [ ] Notifications
- [ ] Initiating PMs
- [ ] Mentioning users
- [ ] Search

and more that I can't think of right now.

If you would like to request a feature, feel free to post a request in the [issues tracker](https://gitlab.com/zaema/minicord/issues).

## Copyright
Copyright (C) 2019-2020 zaema

![](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)

Minicord is licenced under the GNU General Public Licence Version 3, which is a free software licence. See the [licence file](LICENSE) for more info.


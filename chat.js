/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * chat.js contains blessed nodes that make up the chat interface
 */
const blessed = require('blessed');
const screen = require('./screen.js');

var main = blessed.box({ // main window which displays the messages in the current channel
    parent: screen.screen,
    mouse: true,
    top: '13%',
    left: 0,
    width: '100%',
    height: '75%',
    keys: true, // vi keys not enabled for main, since the text box would have a bunch of j and k's in it
    tags: true, // allows styling of messages with {} tags
    alwaysScroll: true,
    scrollable: true
});

var bezel = blessed.form({
    parent: screen.screen,
    content: 'Logging in...', 
    align: 'center',
    top: 0,
    left: 0,
    height: '13%',
    tags: true, // allows styling of messages with {} tags
    border: {
	type: 'line',
    }
});

var menuButton = blessed.box({ // menu button to open up server/channel list
    parent: bezel,
    clickable: true, // make sure mouse: true as well in main
    top: 0,
    left: 0,
    width: '10%',
    style: {
	bg: 'white',
	focus: {
	    inverse: true
	}
    },
});

var textbox = blessed.form({ // the form which contains the user input textbox
    parent: screen.screen,
    keys: true,
    mouse: true,
    inputOnFocus: true,
    top: '90%',
    left: 0,
    height: '15%',
    border: {
	type: 'line',
    }
});

var input = blessed.textbox({
    parent: textbox,
    keys: true,
    mouse: true,
    inputOnFocus: true,
});

module.exports = { main, bezel, menuButton, textbox, input }

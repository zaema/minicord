/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * menucontroller.js contains event and interactions for the menu interface
 */
const screen = require('./screen.js');
const chat = require('./chat.js');
const menu = require('./menu.js');
const api = require('./api.js');
const controller = require('./controller.js');
const model = require('./model.js');

menu.serverList.on('focus', function () { // when serverList is focused, set the label to bold, and vice versa for channeList
    menu.serverList.setLabel("{bold}Servers");
    // set default values for highlighted, as it would have been modified
    menu.serverList.style.selected.fg = 'black';
    menu.serverList.style.selected.bg = 'white';
    
    menu.channelList.setLabel("Channels");
    // invert the selected item's colour scheme for the channels list, so that only the selected item in the server list is highlighted, giving the user a better idea that the server list is currently in focus.
    menu.channelList.style.selected.fg = 'white';
    menu.channelList.style.selected.bg = 'black';
    screen.screen.render();
});

menu.channelList.on('focus', function () { // opposite of above function
    menu.channelList.setLabel("{bold}Channels");
    menu.channelList.style.selected.fg = 'black';
    menu.channelList.style.selected.bg = 'white';
    menu.serverList.setLabel("Servers");
    menu.serverList.style.selected.fg = 'white';
    menu.serverList.style.selected.bg = 'black';
    screen.screen.render();
});

screen.screen.key(['right', 'l', 'C-f'], function () { // moving right using right arrow (hard to do on
    if (chat.main.hidden) {                            // mobile), emacs C-f, vim  l will switch focus to
	menu.channelList.focus();	               // channelList. pressing enter while a server is 
	screen.screen.render();                        // selected should not switch focus to channelList
    }                                                  // immediately, as people may want to inspect the      
});                                                    // channels within a server without going into them.

screen.screen.key(['left', 'h', 'C-b'], function () {  // see above
    if (chat.main.hidden) {
	menu.serverList.focus();
	screen.screen.render();
    } // for some reason, emacs keybinding to move left will reset the channelList selection to the
});   // very top, but ONLY for moving left, not moving right.

screen.screen.key(['C-n', 'j'], function () { // specific emac/vim keys for moving up and down. There are probably cleaner ways to do this
    if (chat.main.hidden) { // bindings for server selection menu
	if(menu.serverList.focused) {
	    menu.serverList.down();
	} else {
	    menu.channelList.down();
	}
	screen.screen.render();
    } // TODO: also try implement emacs scrolling in main
});

screen.screen.key(['C-p', 'k'], function () { // specific emac/vim keys for moving up and down. There are probably cleaner ways to do this
    if (chat.main.hidden) { // bindings for server selection menu
	if(menu.serverList.focused) {
	    menu.serverList.up();
	} else {
	    menu.channelList.up();
	}
	screen.screen.render();
    }
});

// guilds.channels are not in the order that they appear in the electron client
menu.serverList.on('select', (item) => { // 'select' consistutes both a mouse click and an enter key press while 'highlighting' the item.
    if (item.content == "PMs") { // item itself is an element containing like 50 billion parameters. Do item.content to specify the string that the item has, to check if it equals
	menu.menuBezel.setContent(item.content); // display server name on menu bezel
	model.guildName = item.content; // store server name so it can be called later
	api.listPms(item.position.top); // pass the index of the selected item so that api.js can find it in pmTypes array
    } else {
	menu.menuBezel.setContent(item.content);
	model.guildName = item.content;
	api.listChannels(item.position.top); // pass the index of the selected item so that api.js can find it in guildIds array
    }
});

menu.channelList.on('select', (item) => {
    // for some reason the client will also crash if the channel that is clicked on is not readable. Need to figure out how to actually response to failed promises, or simply don't push the non visible channel to the serverlist in the first place.
    if (item.content[0] == "<" || item.content[0] == "=" || item.content == "{bold}The list is empty") {
	// do nothing if there is only one item saying "The list is empty",
	// or if an illegal character is present (indicating it is not a text channel and thus unenterable)
    } else {
	api.enterChannel(item.position.top).then(function(result) { // result is either 0 or 1
	    if (result === 0) { // if success signal is returned (0)
		chat.bezel.setContent(model.guildName + " - " + item.content); // set the title of the bezel to the server + channel name
		model.channelName = item.content;
		chat.input.clearValue();
		controller.toggleInterface();
	    } else {  
		item.content = "{bold}Cannot access channel";
		screen.screen.render();
	    }
	});
    }
});

/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * model.js contains variables that are accessed by many other parts of the program.
 */

var unlock = false; // determines whether to accept inputs. Default is false until the login message displays.

var historyIndex = 0; // records how far back user has scrolled up in a channel's history

var currentGuild = null; // these should be stored as objects to speed up access.
var currentChannel; // this object is needed in order to make it efficient for calling channel related functions, namely when resetting scrollIndex while viewing the history in the chat interface
var currentChannelId = null; // used to check if message will be pushed to main in messageCreate event of the eris client.

var guildIds = []; // Same purpose as channelIds. Store strings of guild IDs to tell different guilds with same names apart.

var channels = []; // Stores an array of channel objects.
// name: name of the channel
// id: id of the channel
// type: type of the channel (4 == category header)
// position: position in relation with other channels under the same category
// parent: first used to store the parent id of its category, then after iterating, it stores the index of the category

var channelIds = []; // still used by listPms(), TODO is to update listPms to use the channels array as well

var pmTypes = []; // the object used to store the type of PM channel, in order to use the right function to fetch the messages.

var guildName = null; // names used to label the bezels
var channelName = null;

module.exports = { unlock, historyIndex, currentGuild, currentChannel, currentChannelId, guildIds, channels, channelIds, pmTypes, guildName, channelName }

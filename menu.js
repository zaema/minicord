/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * menu.js contains blessed nodes that make up the server/channel selection menu
 */
const blessed = require('blessed');
const screen = require('./screen.js');

let menuBezel = blessed.form({ // server topbar which has button to return to main view i.e. current channel
    hidden: true,
    parent: screen.screen,
    content: "'enter' to select, mouse/arrow/vim/emacs keys to navigate",
    align: 'center',
    top: 0,
    left: 0,
    height: '13%',
    border: {
	type: 'line',
    }
});

var backButton = blessed.box({ // back button which returns to channel msg
    parent: menuBezel,
    clickable: true,  //make sure mouse: true as well in windows you want to scroll
    top: 0,
    left: '90%',
    style: {
	bg: 'white',
	focus: {
	    inverse: true
	}
    }
});

let serverList = blessed.list({
    hidden: true,
    mouse: true,
    label: 'Servers',
    tags: true, // this allows setLabel to change label to bold text
    parent: screen.screen,
    top: '15%',
    left: 0,
    width: '50%',
    height: '90%',
    border: {
	type: 'line'
    },
    style: {
	selected: { // makes it so that "hovered"(i.e. selected) items on the list are highlighted
	    fg: 'black',
	    bg: 'white'
	},
	focus: { // makes it so that the border of the list is highlighted when it is selected
	    border: {
		bold: true
	    }
	}
    },
    keys: true,
    //vi: true,
    alwaysScroll: true,
    scrollable: true
});

let channelList = blessed.list({
    hidden: true,
    mouse: true,
    label: 'Channels',
    tags: true, // this allows setLabel to change label to bold text
    parent: screen.screen,
    top: '15%',
    left: '50%',
    width: '50%',
    height: '90%',
    border: {
	type: 'line'
    },
    style: {
	selected: {  // makes it so that "hovered"(i.e. selected) items on the list are highlighted
	    fg: 'black',
	    bg: 'white'
	},
	focus: { // makes it so that the border of the list is highlighted when it is selected
	    border: {
		bold: true
	    }
	}
    },
    keys: true,
    //vi: true,
    alwaysScroll: true,
    scrollable: true
});

module.exports = { menuBezel, backButton, serverList, channelList }

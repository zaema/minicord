/* 
 * Minicord - A lightweight discord terminal client for android
 * Copyright (C) 2019-2020 zaema
 *
 * This file is part of Minicord.
 *
 * Minicord is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * Minicord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * commands.js contains commands used to interact with the minicord client
 */
const api = require('./api.js');
const chat = require('./chat.js');
const screen = require('./screen.js');

async function processCommand(input) {
    input = input.split(" ");
    var command = input[0].toLowerCase();
    var args;
    var output;

    if (input.length > 1) { // if there are arguments for the command
	args = input.slice(1); // slice off the command and store remainder of arguments into array of args
    } else {
	args = [null];
    }
    
    switch(command) {
    case "help":
	switch(args[0]) { // second switch to parse args
	case null:
	    output = "\n{bold}Use the {white-bg}{black-fg}arrow keys{/white-bg}{/black-fg} to move up or down one line. Use the {white-bg}{black-fg}mouse wheel{/white-bg}{/black-fg} or your fingers (if you are using touchscreen) to scroll up and down the chat.\n\nPress {white-bg}{black-fg}'tab'{/white-bg}{/black-fg} or click the buttons at the top to switch between the server/channel selection and the chat interface.\n\nPress {white-bg}{black-fg}'enter'{/white-bg}{/black-fg} to start typing into the input box and {white-bg}{black-fg}'esc'{/white-bg}{/black-fg} to escape from the input box. Pressing {white-bg}{black-fg}'enter'{/white-bg}{/black-fg} while inside the input box will submit your message.\n\nYou can navigate the servers and channels selection screen with {white-bg}{black-fg}arrow keys{/white-bg}{/black-fg}, {white-bg}{black-fg}vim keys (h, j, k, l){/white-bg}{/black-fg}, or {white-bg}{black-fg}emacs keys (C-f, C-b, C-p, C-n){/white-bg}{/black-fg}. Press {white-bg}{black-fg}'enter'{/white-bg}{/black-fg} to confirm your server/channel selection.\n\nYou can use the {white-bg}{black-fg}'up'{/white-bg}{/black-fg} or {white-bg}{black-fg}'down'{/white-bg}{/black-fg} arrows when scrolled to the top/bottom of the messages list to view the past messages of a channel, 50 messages at a time. Using {white-bg}{black-fg}'Ctrl-p'{/white-bg}{/black-fg} or {white-bg}{black-fg}'Ctrl-n'{/white-bg}{/black-fg} allows you to go through a channel's history quickly without having to scroll to the top or bottom.\n\nYou can exit the program by pressing {white-bg}{black-fg}'Ctrl-C'{/white-bg}{/black-fg}.\n\nType {white-bg}{black-fg}/help{/white-bg}{/black-fg} to repeat this message, or type {white-bg}{black-fg}/commands{/white-bg}{/black-fg} to see the available commands.\n\n{/}";
	    break;
	    
	case "help":
	    output = "{white-bg}{black-fg}/help{/}{bold}: for general help for using minicord, {/}{white-bg}{black-fg}/help [command]{/}{bold}: find out how to use a specific command.{/}";
	    break;

	case "commands":
	    output = "{white-bg}{black-fg}/commands{/}{bold}: show a list of all available commands.{/}";
	    break;

	    /* See the comments on the main case for the "joinserver" command to see why its disabled
	     *
	case "joinserver":
	    output = "{white-bg}{black-fg}/joinserver [inviteId]{/}{bold}: Join a server from the inputted invite link ID (the alphanumeric character sequence after the discord.gg/ in the invite link); This feature is broken right now.{/}";
	    break;
	    */

	case "echo":
	    output = "{white-bg}{black-fg}/echo [args]{/}{bold}: echoes the arguments inputted after the command.{/}";
	    break;
	    
	default:
	    output = "{white-bg}{black-fg}\"" + args + "\"{/} is not a valid command. Please do {white-bg}{black-fg}/help [command]{/} to find help for a specific command, or {white-bg}{black-fg}/help for general help.{/}";
	    break;
	}
	break;
	
    case "commands":
	output = "{bold}Available Commands: {/}";
	output += "{white-bg}{black-fg}/help{/}, ";
	output += "{white-bg}{black-fg}/commands{/}, ";
	// output += "{white-bg}{black-fg}/joinserver{/}, ";
	output += "{white-bg}{black-fg}/echo{/}";
	break;

	/* While the Eris.js framework allows joining servers via invite links when using a user token,
	 * the request signature and headers sent from the join request will be flagged by discord's
	 * centralized servers as a bot and rejected. Hence it is not possible to join servers without
	 * spoofing the request headers, which I do not know how to do yet.
	 *
    case "joinserver":
	if (args[0] === null) {
	    output = "{bold}You need to put in an invite ID!{/}";
	} else {
	    var result = await api.acceptInvite(args[0]);
	    if (result === "1") { // this is a string because acceptInvite resolve promise as a string
		output = "{bold}Failed to join server. Please make sure the invite link is valid.{/}";
	    } else {
		output = "{bold}Success! You have joined " + result;
	    }
	}
	break;
	*/

    case "echo": //echoes args (as a test)
	output = args.toString();
	break;
	
    default:
	output = "{bold}Invalid command. Do {/}{white-bg}{black-fg}/commands{/} {bold}to see the list of available commands.{/}";
	break;
    }
    
    //return output;
    chat.main.pushLine(output);
    chat.input.clearValue();
    chat.main.focus(); // possibly redundant, as the text input box will unfocus automatically.
    chat.main.setScrollPerc(100);
    screen.screen.render();
    Promise.resolve(); // not sure if this needs to be here
}

module.exports = { processCommand }
